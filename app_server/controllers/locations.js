var request = require('request');
var apiOptions = {
  server: "http://localhost:3000"
};
if(process.env.NODE_ENV == 'production'){
  apiOptions.server = "https://limitless-beyond-67641.herokuapp.com";
}


var _showError = function (req, res, status) {
  var title, content;
  if (status === 404) {
    title = "404, page not found";
    content = "Oh dear. Looks like we can't find this page. Sorry.";
  } else if (status === 500) {
    title = "500, internal server error";
    content = "How embarrassing. There's a problem with our server.";
  } else {
    title = status + ", something's gone wrong";
    content = "Something, somewhere, has gone just a little bit wrong.";
  }
  res.status(status);
  res.render('generic-text', {
    title : title,
    content : content
  });
};

var renderHomepage = function(req, res, responseBody){
  res.render('locations-list', { title: 'HelpCar - Find a place to work with wifi',
      pageHeader: {
        title: 'HelpCar',
        strapline: 'Find places to work with wifi near you!'
      },
      sidebar: 
        "Locing for Wifi and seat? HelpCar helps you find places to work when out and about. Perhaps with coffee, cake or a pint? Let HelpCar help you find the place you're looking for."       
  });
};

/* GET 'home' page */
module.exports.homelist = function(req, res) {
  renderHomepage(req, res);
};


var getLocationInfo = function (req, res, callback) {
  var requestOptions, path;
  path = "/api/locations/" + req.params.locationid;
  requestOptions = {
    url : apiOptions.server + path,
    method : "GET",
    json : {}
  };
  request(
    requestOptions,
    function(err, response, body) {
      var data = body;
      if (response.statusCode === 200) {
        data.coords = {
          lng : body.coords[0],
          lat : body.coords[1]
        };
        callback(req, res, data);
      } else {
        _showError(req, res, response.statusCode);
      }
    }
  );
};

var renderDetailPage = function (req, res, locDetail) {
  res.render('location-info', {
    title: locDetail.name,
    pageHeader: {title: locDetail.name},
    sidebar: {
      context: 'is on HelpCar because it has accessible wifi and space to sit down with your laptop and get some work done.',
      callToAction: 'If you\'ve been and you like it - or if you don\'t - please leave a review to help other people just like you.'
    },
    location: locDetail
  });
};

/* GET 'Location info' page */
module.exports.locationInfo = function(req, res){
  getLocationInfo(req, res, function(req, res, responseData) {
    renderDetailPage(req, res, responseData);
  });
};


var renderReviewForm = function(req, res, locDetail) {
  res.render('location-review-form', {
        title: 'Review ' + locDetail.name + ' on HelpCar',
        pageHeader: { title: 'Review ' + locDetail.name},
        error: req.query.err,
        url: req.originalUrl
    });
};

/* GET 'Add review' page */
module.exports.addReview = function(req, res) {
  getLocationInfo(req, res, function(req, res, responseData) {
    renderReviewForm(req, res, responseData);
  });
};

module.exports.doAddReview = function(req, res){
  var requestOptions, path, locationid, postdata;
  locationid = req.params.locationid;
  path = "/api/locations/" + locationid + '/reviews';
  postdata = {
    author: req.body.name,
    rating: parseInt(req.body.rating, 10),
    reviewText: req.body.review
  };
  requestOptions = {
    url : apiOptions.server + path,
    method : "POST",
    json : postdata
  };

  if(!postdata.author || !postdata.rating || !postdata.reviewText){
    res.redirect('/location/' + locationid + '/reviews/new?err=val');
  }else{
    request(
      requestOptions, 
      function(err, response, body){
        if (response.statusCode === 201){
          res.redirect('/location/' + locationid);
        }else if(response.statusCode === 400 && body.name && body.name === "ValidationError"){
          res.redirect('/location/' + locationid + '/reviews/new?err=val');
        }else{
          _showError(req, res, response.statusCode);
        }
      }
    );
  }
  
};


/* POST NEW LOCATION */


var renderLocationForm = function(req, res){
  res.render('location-form', { 
      title: 'New Location on HelpCar',
        pageHeader: { title: 'New location'}
  });
};

module.exports.AddLocation = function(req, res) {
    renderLocationForm(req, res);
};

module.exports.doAddLocation = function(req, res) {
  var requestOptions, path, postdata;
  path = "/api/locations";
  postdata = {
    name: req.body.name,
    address: req.body.address,
    services: req.body.services,
    lng: req.body.lng, 
    lat: req.body.lat,
    days1: req.body.days1,
    opening1: req.body.opening1,
    closing1: req.body.closing1,
    days2: req.body.days2,
    opening2: req.body.opening2,
    closing2: req.body.closing2
  };
  requestOptions = {
    url : apiOptions.server + path,
    method : "POST",
    json : postdata
  };
  request(
    requestOptions, 
    function(err, response, body){
      if (response.statusCode === 201){
        res.redirect('/');
      }else{
        _showError(req, res, response.statusCode);
      }
    }
  );
};