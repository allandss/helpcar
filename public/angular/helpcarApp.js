// Document JS

angular.module('helpcarApp', []);

var _isNumeric = function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n); 
};

var formatDistance = function(){
	return function (distance){
		var numDistance, unit;
		if (distance && _isNumeric(distance)) {
			if (distance > 1000) {
		      numDistance = parseFloat(distance / 1000).toFixed(1);
		      unit = 'km';
		 	} else {
		      numDistance = parseInt(distance * 1);
		      unit = 'm';
		    }
		    return numDistance + unit;
		} else {
		    return "?";
		}
	};
};

var ratingStars = function(){
	return {
		scope : {
			thisRating: '=rating'
		},
		templateUrl: '/angular/rating-stars.html'	
	};
};



var geolocation = function (){
	var getPosition = function(cbSuccess, cbError, cbNoGeo){
		if(navigator.geolocation){
			navigator.geolocation.getCurrentPosition(cbSuccess, cbError);
		}else {
			cbNoGeo();
		}
	};
	return {
		getPosition : getPosition
	};
};



var locationListCtrl = function($scope, helpcarData, geolocation){
	$scope.message = "Checking Your location";

	$scope.getData = function(position) {
		var lat = position.coords.latitude, 
			lng = position.coords.longitude;
		$scope.message = "Searching for nearby places";

		helpcarData.locationsByCoords(lat, lng)
			.success(function(data) {
				$scope.message = data.length > 0 ? "" : "Not locations found";
				$scope.data = { locations: data };
			})
			.error(function (e) {
				$scope.message = "Sorry, something's gone wrong";
				console.log(e);
			});
	};

	$scope.showError = function(error){
		$scope.$apply(function(){
			$scope.message = error.message;
		});
	};

	$scope.noGeo = function(){
		$scope.apply(function(){
			$scope.message = "Geolocation not supported by this browser.";
		});
	};

	geolocation.getPosition($scope.getData,$scope.showError,$scope.noGeo);
	
};

var helpcarData = function($http){
	var locationsByCoords = function(lat, lng){
		return $http.get('/api/locations?lng='+lng+'&lat='+lat+'&maxDistance=14000');
	};
	return {
		locationsByCoords : locationsByCoords
	};
	
};

angular
	.module('helpcarApp')
	.controller('locationListCtrl', locationListCtrl)
	.filter('formatDistance', formatDistance)
	.directive('ratingStars', ratingStars)
	.service('helpcarData', helpcarData)
	.service('geolocation', geolocation);
