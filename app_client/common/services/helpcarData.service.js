(function(){

angular
	.module('helpcarApp')
	.service('helpcarData', helpcarData);

	helpcarData.$inject = ['$http'];
	function helpcarData ($http){
		var locationsByCoords = function(lat, lng){
			return $http.get('/api/locations?lng='+lng+'&lat='+lat+'&maxDistance=50000');
		};

		var locationById = function(locationid){
			return $http.get('/api/locations/' + locationid);
		};

		var addReviewById = function(locationid, data){
			return $http.post('/api/locations/' + locationid + '/reviews', data);
		};

		return {
			locationsByCoords : locationsByCoords,
			locationById : locationById,
			addReviewById : addReviewById
		};
		
	}

})();
