(function(){
	angular
		.module('helpcarApp')
		.controller('locationDetailCtrl', locationDetailCtrl);

	locationDetailCtrl.$inject = ['$routeParams', '$uibModal', 'helpcarData'];
	function locationDetailCtrl ($routeParams, $uibModal, helpcarData){
		var vm = this;
		vm.locationid = $routeParams.locationid;

		helpcarData.locationById(vm.locationid)
			.success(function(data){
				vm.data = { 
					location: data 
				};
				vm.pageHeader = {
					title: vm.data.location.name 
				};
			})
			.error(function (e) {
				console.log(e);
			});

		vm.popupReviewForm = function (){
			var ModalInstance = $uibModal.open({
				templateUrl: '/reviewModal/reviewModal.view.html',
				controller: 'reviewModalCtrl as vm',
				resolve: {
					locationData : function(){
						return {
							locationid : vm.locationid,
							locationName : vm.data.location.name
						};
					}
				}
			});
			ModalInstance.result.then(function (data) {
				vm.data.location.reviews.push(data);
			});
		};
	}
})();