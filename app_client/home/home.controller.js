if(window.location.pathname !== '/'){
	window.location.href = '/#' + window.location.pathname;
}

(function (){

angular
	.module('helpcarApp')
	.controller('homeCtrl', homeCtrl);
 homeCtrl.$inject = ['$scope', 'helpcarData', 'geolocation'];
function homeCtrl ($scope, helpcarData, geolocation) {
	var vm = this;
	vm.pageHeader = {
		title: 'HelpCar',
		strapline: 'Find places to work with wifi near you!'
	};
	vm.sidebar = {
		content: 'Locing for Wifi and seat? HelpCar helps you find places to work when out and about. Perhaps with coffee, cake or a pint? Let HelpCar help you find the place you\'re looking for.'
	};

	vm.message = "Checking Your location";

	vm.getData = function(position) {
		var lat = position.coords.latitude, 
			lng = position.coords.longitude;
		vm.message = "Searching for nearby places";

		helpcarData.locationsByCoords(lat, lng)
			.success(function(data) {
				vm.message = data.length > 0 ? "" : "Not locations found nearby";
				vm.data = { locations: data };
			})
			.error(function (e) {
				vm.message = "Sorry, something's gone wrong";
				console.log(e);
			});
	};

	vm.showError = function(error){
		$scope.$apply(function(){
			vm.message = error.message;
		});
	};

	vm.noGeo = function(){
		$scope.apply(function(){
			vm.message = "Geolocation not supported by this browser.";
		});
	};

	geolocation.getPosition(vm.getData,vm.showError,vm.noGeo);
}

})();