var mongoose = require('mongoose');

var reviewSchema = new mongoose.Schema({
    author: {type: String, required: true},
    rating: {type: Number, required: true, min: 0, max: 5},
    reviewText: {type: String, required: true},
    createdOn: {type: Date, "default": Date.now}
});

var locationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    address: String,
    zipCode: String,
    phone: String,
    rating: {
        type: Number,
        "default": 0,
        min: 0,
        max: 5
    },
    services: [String],
    // Always store coordinates longitude, latitude order.
    coords: {
        type: [Number],
        index: '2dsphere',
        "2dsphereIndexVersion": 2,
        required: true
    },
    operatingHours: String,
    reviews: [reviewSchema]
});

mongoose.model('Location', locationSchema);

//db.locations.save({name: 'Speed Up', address: 'R Coronel Domingos Ferreira, 158 - Vila Firmiano Pinto - São Paulo, SP', zipCode: '04125-200', phone: '(11) 5061-3824', rating: 1, services: ['Reboque', 'Troca de pneu', 'Carga em bateria'], coords: [-46.6173476, -23.5984636], operatingHours: '24 Horas' })

//db.locations.update({name: 'Speed Up'}, { $push: {reviews: {author: 'Jakeline Nogueira', _id: ObjectId(), rating: 5, timestamp: new Date("Abr 30, 2018"), reviewText: "Ótimo atendimento"}}})

//db.locations.remove({name: 'Speed Up'})