/* Conexão com banco de dados */

var mongoose = require( 'mongoose' );
var gracefulShutdown;
var dbURI = 'mongodb://localhost/helpCar';

mongoose.connect(dbURI);

mongoose.connection.on('connected', function(){
	console.log('Mongoose está conectado com ' + dbURI);
});

mongoose.connection.on('error', function(err){
	console.log('Mongoose está com erro: ' + err);
});

mongoose.connection.on('disconnected', function(){
	console.log('Mongoose está desconetado.');
});

var gracefulShutdown = function (msg, callback) {
	mongoose.connection.close(function(){
		console.log('Mongoose desconectado através de ' + msg);
		callback();
	});
};

process.once('SIGUSR2', function(){
	gracefulShutdown('nodemon restart', function (){
		process.kill(process.pid, 'SIGUSR2');
	});
});

process.on('SIGINT', function(){
	gracefulShutdown('app termination', function (){
		process.exit(0);
	});
});

process.on('SIGTERM', function(){
	gracefulShutdown('Heroku app shutdown', function (){
		process.exit(0);
	});
}); 

require('./locations');
